package com.whiteklay;

import org.apache.avro.generic.GenericRecord;

public class Record {
    GenericRecord value;
    String key;
    long offset;

    public GenericRecord getValue() {
        return value;
    }

    public void setValue(GenericRecord value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getOffset() {
        return offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public Record(GenericRecord value, String key, long offset) {
        this.value = value;
        this.key = key;
        this.offset = offset;
    }
}
